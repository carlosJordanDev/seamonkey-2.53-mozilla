/* Any copyright is dedicated to the Public Domain.
   http://creativecommons.org/publicdomain/zero/1.0/ */

ChromeUtils.import("resource://testing-common/services/common/utils.js");

function run_test() {
  let thing = {o: {foo: "foo", bar: ["bar"]}, a: ["foo", {bar: "bar"}]};
  let ret = TestingUtils.deepCopy(thing);
  Assert.notEqual(ret, thing)
  Assert.notEqual(ret.o, thing.o);
  Assert.notEqual(ret.o.bar, thing.o.bar);
  Assert.notEqual(ret.a, thing.a);
  Assert.notEqual(ret.a[1], thing.a[1]);
  Assert.equal(ret.o.foo, thing.o.foo);
  Assert.equal(ret.o.bar[0], thing.o.bar[0]);
  Assert.equal(ret.a[0], thing.a[0]);
  Assert.equal(ret.a[1].bar, thing.a[1].bar);
}
