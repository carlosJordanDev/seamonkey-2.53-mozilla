rustup default nightly-pc-windows-msvc

cd ../../toolkit/library/rust
cargo update -p cssparser:0.19.5 --precise 0.19.5 -Z minimal-versions
cargo update -p parking_lot_core:0.2.4 --precise 0.2.4 -Z minimal-versions
cargo update -p smallvec:0.4.5 --precise 0.4.5 -Z minimal-versions

cd ../gtest/rust
cargo update -p cssparser:0.19.5 --precise 0.19.5 -Z minimal-versions
cargo update -p parking_lot_core:0.2.4 --precise 0.2.4 -Z minimal-versions
cargo update -p smallvec:0.4.5 --precise 0.4.5 -Z minimal-versions
