--- nsReadableUtils.cpp
+++ nsReadableUtils.cpp
@@ -46,23 +46,23 @@ char* ToNewCString(const nsAString& aSou
   return dest;
 }
 
 char* ToNewUTF8String(const nsAString& aSource, uint32_t* aUTF8Count) {
   auto len = aSource.Length();
   // The uses of this function seem temporary enough that it's not
   // worthwhile to be fancy about the allocation size. Let's just use
   // the worst case.
-  // Times 3 plus 2, because ConvertUTF16toUTF8 requires times 3 plus 1 and
+  // Times 3 plus 1, because ConvertUTF16toUTF8 requires times 3 and
   // then we have the terminator.
   // Using CheckedInt<uint32_t>, because aUTF8Count is uint32_t* for
   // historical reasons.
   mozilla::CheckedInt<uint32_t> destLen(len);
   destLen *= 3;
-  destLen += 2;
+  destLen += 1;
   if (!destLen.isValid()) {
     return nullptr;
   }
   size_t destLenVal = destLen.value();
   char* dest = static_cast<char*>(moz_xmalloc(destLenVal));
 
   size_t written = ConvertUTF16toUTF8(aSource, MakeSpan(dest, destLenVal));
   dest[written] = 0;
